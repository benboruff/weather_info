defmodule CLITest do
  use ExUnit.Case, async: true

  import WeatherInfo.CLI, only: [parse_args: 1]

  test ":help should be returned by option parsing with -h and --help options" do
    assert parse_args(["-h", "foobar"]) == :help
    assert parse_args(["--help", "foobar"]) == :help
  end

  test "two values retuned if two given" do
    assert parse_args(["lat", "long"]) == {"lat", "long"}
  end
end
