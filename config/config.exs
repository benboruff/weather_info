import Mix.Config
config :weather_info, weatherapi_url: "https://api.weather.gov"

config :logger, compile_time_purge_level: :info
