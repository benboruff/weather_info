defmodule WeatherInfo.TableFormatter do
  def print_table_for_columns(rows, header, {lat, long}) do
    table_title = "Forecast for lat: #{lat}, long:  #{long}"
    forecasts = for row <- rows, do: [row[:name], row[:forecast], row[:temperature]]

    TableRex.quick_render!(forecasts, header, table_title)
    |> IO.puts()
  end
end
