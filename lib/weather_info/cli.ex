defmodule WeatherInfo.CLI do
  import WeatherInfo.TableFormatter, only: [print_table_for_columns: 3]

  @moduledoc """
  Handle the command line parsing and the dispatch to
  the various functions that end up generating a
  table of the two most current weather forecasts and
  temperature for a given latitude and longitude.
  """

  def main(argv) do
    argv
    |> parse_args
    |> process
  end

  @doc """
  `argv` can be -h or --help, which returns :help.
  Otherwise it is a latitude, and a longitude.
  Return a tuple of `{ lat, long }`, or `:help` if help was given.
  """

  def parse_args(argv) do
    OptionParser.parse(argv, switches: [help: :boolean], aliases: [h: :help])
    |> elem(1)
    |> args_to_internal_representation
  end

  def args_to_internal_representation([lat, long]) do
    {lat, long}
  end

  def args_to_internal_representation(_) do
    :help
  end

  def process(:help) do
    IO.puts("""
    usage: weather_info <latitude> <longitude>
    """)

    System.halt(0)
  end

  def process({lat, long}) do
    WeatherInfo.WeatherApi.fetch_forecast(lat, long)
    |> decode_response
    |> sort_into_ascending_order
    |> print_table_for_columns(["When", "Forecast", "Temperature"], {lat, long})
  end

  def decode_response({:ok, forecast_data}) do
    forecast_data
  end

  def decode_response({:error, error}) do
    IO.puts("Error fetching weather info: #{error["message"]}")
    System.halt(2)
  end

  def sort_into_ascending_order(periods) do
    periods
    |> Enum.sort(fn period1, period2 -> period1[:number] < period2[:number] end)
  end
end
