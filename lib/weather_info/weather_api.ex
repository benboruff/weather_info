defmodule WeatherInfo.WeatherApi do
  require Logger
  @user_agent [{"User-Agent", "ben@boruff.me"}]
  @weatherapi_url Application.get_env(:weather_info, :weatherapi_url)

  def fetch_forecast(lat, long) do
    Logger.debug("Fetching #{lat} and #{long} from NOAA API")

    weatherapi_url(lat, long)
    |> HTTPoison.get(@user_agent)
    |> handle_top_level_response
  end

  def weatherapi_url(lat, long) do
    Logger.debug("#{@weatherapi_url}/points/#{lat},#{long}")
    "#{@weatherapi_url}/points/#{lat},#{long}"
  end

  def handle_top_level_response({_, %{status_code: status_code, body: body}}) do
    Logger.debug("Got response: status code=#{status_code}")
    # Logger.debug(fn -> inspect(body) end)

    body
    |> Poison.Parser.parse!(%{})
    |> get_forecast
  end

  defp check_status(200), do: :ok
  defp check_status(_), do: :error

  defp get_forecast(body) do
    body["properties"]["forecast"]
    |> HTTPoison.get(@user_agent)
    |> handle_low_level_response
  end

  defp handle_low_level_response({_, %{status_code: status_code, body: body}}) do
    {
      status_code |> check_status,
      body |> Poison.Parser.parse!(%{}) |> get_periods
    }
  end

  defp get_periods(data) do
    data["properties"]["periods"]
    |> select_periods_one_and_two
  end

  defp select_periods_one_and_two(periods) do
    for period <- periods,
        period["number"] == 1 or period["number"] == 2,
        do: [
          number: period["number"],
          name: period["name"],
          forecast: period["shortForecast"],
          temperature: Integer.to_string(period["temperature"])
        ]
  end
end
