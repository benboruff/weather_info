defmodule WeatherInfo do
  @moduledoc """
  Documentation for WeatherInfo.
  """

  @doc """
  Hello world.

  ## Examples

      iex> WeatherInfo.hello()
      :world

  """
  def hello do
    :world
  end
end
