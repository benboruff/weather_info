defmodule WeatherInfo.MixProject do
  use Mix.Project

  def project do
    [
      app: :weather_info,
      escript: escript_config(),
      version: "0.1.0",
      name: "WeatherInfo",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.5.0"},
      {:poison, "~> 4.0.1"},
      {:ex_doc, "~> 0.20.2"},
      {:earmark, "~> 1.3.2"},
      {:table_rex, "~> 2.0.0"}
    ]
  end

  defp escript_config do
    [
      main_module: WeatherInfo.CLI
    ]
  end
end
